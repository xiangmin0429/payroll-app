package com.payroll.unitTest.servlet;

import com.payroll.servlet.EmployeeServlet;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import javax.servlet.RequestDispatcher;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import static org.mockito.Mockito.*;

public class EmployeeServletTest {
    @Mock private HttpServletRequest request;
    @Mock private HttpServletResponse response;
    @Mock private RequestDispatcher requestDispatcher;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testGet() throws Exception {
        // Arrange
        when(request.getRequestDispatcher(anyString())).thenReturn(requestDispatcher);

        // Act
        new EmployeeServlet().doGet(request, response);

        // Assert
        verify(request).getRequestDispatcher("employee.jsp");
        verify(request).setAttribute(eq("employees"), anyList());
    }
}
