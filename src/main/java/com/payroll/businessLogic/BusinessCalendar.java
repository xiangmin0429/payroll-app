package com.payroll.businessLogic;

import java.util.Date;

/**
 * Business Calendar
 */
public interface BusinessCalendar {
    /**
     * Determine given date is holiday or not
     * @param date given date
     * @return true: is holiday
     */
    boolean isNationalHoliday(Date date);
}